<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
  <a class="navbar-brand" href="#">Simple Blog</a>

  <ul class="navbar-nav ml-auto">
    <li class="nav-item active">
      <a class="nav-link" href="<?php echo base_url(); ?>">HOME</a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo base_url('dashboard/post'); ?>">DASHBOARD</a>
    </li>
  </ul>
</nav>
<div class="jumbotron">
  <div class="container">
    <div class="row">
      <div class="col-2">
        <img src="<?php echo base_url('assets/img/logo.png'); ?>" width="150px" alt="">
      </div>
      <div class="col-8">
        <h4 class="display-4 pt-4">Welcome to my blog</h4>
        <p>This is a simple blog or informational website. </p>
      </div>
    </div>
  </div>
</div>