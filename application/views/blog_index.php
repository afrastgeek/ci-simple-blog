<div class="container">
  <div class="row">
    <?php if ($entries) : ?>
      <?php foreach ($entries as $data) : ?>
        <div class="col-md-4">
          <h2><?php echo $data->title; ?></h2>
          <p><?php echo $data->createDate; ?></p>
          <p><?php echo substr($data->body, 0, 150)."..."  ?></p>
          <p><a class="btn btn-primary" href="<?php echo site_url('blog/post/' . $data->id); ?>" role="button">Read more &raquo;</a></p>
        </div>
      <?php endforeach; ?>
    <?php else : ?>
      <div class="col-12">
        <div class="alert alert-primary" role="alert">
        No post results found.
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>
