<div class="container">
  <div class="row">
    <div class="col-7">
      <h3>Dashboard</h3>
    </div>
    <div class="col-5">
      <form action="<?php echo base_url('dashboard/post'); ?>" method="get" class="form-inline">
        <div class="input-group">
          <input type='text' name='search' class='form-control' value="<?php echo isset($_GET['search']) ? $_GET['search'] : ""; ?>">
          <span class="input-group-btn">
            <button class="btn btn-primary" type="submit">Search</button>
          </span>
        </div>
        &nbsp;<a href="<?php echo base_url('dashboard/post'); ?>" class="btn btn-secondary">Cancel</a>
      </form>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="table-responsive pt-3">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th width="500px">Body</th>
              <th width="110px">Posted</th>
              <th width="130px">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 0; if ($entries) : ?>
              <?php foreach ($entries as $data) : ?>
                <tr>
                  <td><?php echo ++$i; ?></td>
                  <td><?php echo $data->title; ?></td>
                  <td><?php echo substr($data->body, 0, 150)."..."  ?></td>
                  <td><?php echo $data->createDate; ?></td>
                  <td>
                    <a class="btn btn-secondary btn-sm" href="<?php echo base_url('dashboard/post/edit/' . $data->id); ?>">Edit</a>
                    <a class="btn btn-primary btn-sm" href="<?php echo base_url('dashboard/post/delete/' . $data->id); ?>">Delete</a>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else : ?>
              <tr class="col-12">
                <td colspan="5" class="alert alert-primary" role="alert">
                  No post results found.
                </td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
