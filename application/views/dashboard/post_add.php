<div class="container">
  <div class="row">
    <div class="col-12">
      <h3>Add New Post</h3>
      <div class="container">
        <div class="col-md-8 pt-4">
          <form method="post" action="<?php echo base_url('dashboard/post/add'); ?>">
          <div class="form-group row">
            <label for="title" class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="title" id="title" autofocus required>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputName" class="col-sm-2 col-form-label">Body</label>
            <div class="col-sm-10">
              <textarea name="body" class="form-control" rows="6" required></textarea>
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Add</button>
          <a href="<?php echo base_url('dashboard/post'); ?>" class="btn btn-secondary">Back</a>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
