<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	/**
	 * Class Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('blog_model');
	}

	/**
	 * Index Page.
	 */
	public function index()
	{
		$data['entries'] = $this->blog_model->get_all_entries();

		$this->load->view('partials/header');
		$this->load->view('partials/navbar');
		$this->load->view('blog_index', $data);
		$this->load->view('partials/footer');
	}

	/**
	 * Post Page
	 *
	 * @param      int  $id     Post identifier
	 */
	public function post($id)
	{

		$data['entry'] = $this->blog_model->get_entry($id)[0];

		$this->load->view('partials/header');
		$this->load->view('partials/navbar');
		$this->load->view('blog_entry', $data);
		$this->load->view('partials/footer');
	}

}
