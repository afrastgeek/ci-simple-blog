<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

    /**
     * Class Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('blog_model');
    }

    /**
     * Index Page.
     */
    public function index()
    {
        $filter = array();

        if (!empty($this->input->get('search'))) {
            $filter = $this->input->get('search');
        }

        $data['entries'] = $this->blog_model->get_all_entries($filter);

        $this->load->view('partials/header');
        $this->load->view('partials/dashboard_navbar');
        $this->load->view('dashboard/post_index', $data);
        $this->load->view('partials/footer');
    }

    /**
     * Index Page.
     */
    public function add()
    {
        $data = $this->input->post();
        if (!empty($data)) {
            $this->blog_model->add_entry($data);
            redirect('dashboard/post');
        }

        $this->load->view('partials/header');
        $this->load->view('partials/dashboard_navbar');
        $this->load->view('dashboard/post_add');
        $this->load->view('partials/footer');
    }

    /**
     * Edit Page.
     *
     * @param      int  $id     The entry identifier
     */
    public function edit($id)
    {
        $data = $this->input->post();
        if (!empty($data)) {
            $this->blog_model->edit_entry($data);
            redirect('dashboard/post');
        }

        $entry = $this->blog_model->get_entry($id)[0];

        $this->load->view('partials/header');
        $this->load->view('partials/dashboard_navbar');
        $this->load->view('dashboard/post_edit', $entry);
        $this->load->view('partials/footer');
    }

    /**
     * Delete Entry
     *
     * @param      int   $id     The entry identifier
     */
    public function delete($id)
    {
        $this->blog_model->delete_entry($id);
        redirect('dashboard/post');
    }

}
