<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_model extends CI_Model {

	/**
	 * Gets all entries.
	 *
	 * @return     stdObj  All entries.
	 */
	public function get_all_entries($filter = NULL)
	{
		if (!empty($filter)) {
			$this->db->like('title', $filter);
			$this->db->or_like('body', $filter);
		}
		$query = $this->db->get('post');

		return $query->result();
	}

	/**
	 * Gets the entry.
	 *
	 * @param      int     $id     Post Identifier
	 *
	 * @return     stdObj  The blog entry.
	 */
	public function get_entry($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('post');

		return $query->result();
	}

	/**
	 * Adds an entry.
	 *
	 * @param      array  $data   The data
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function add_entry($data)
	{
		if ($data)
		{
			$data['createDate'] = date("Y-m-d");

			$this->db->insert('post', $data);
		}
	}

	/**
	 * Edit and entry.
	 *
	 * @param      array  $data   The entry data
	 */
	public function edit_entry($data)
	{
		if ($data)
		{
			$data['createDate'] = date("Y-m-d");

			$this->db->where('id', $data['id']);
			$this->db->update('post', $data);
		}
	}

	public function delete_entry($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('post');
	}

}
